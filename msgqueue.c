#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <assert.h>
#include <stdint.h>
#include <errno.h>
#include <stdlib.h>

typedef struct msg_t
{
    long msg_n;
} msg_t;

#define $(code)         \
    do                  \
    {                   \
        errno = 0;      \
        code;           \
        perror( #code );\
    } while( 0 )

int child_introduction( int qid, long n )
{
    msg_t msg = {};
    $(msgrcv( qid, &msg, 0, n + 1, 0 ));

    printf( "%d ", n );
    fflush( stdout );
    msg.msg_n--;
    $(msgsnd( qid, &msg, 0, 0 ));

    exit( EXIT_SUCCESS );
}

int main( int argc, char* argv[] )
{
    long N = 0;

    if( argc != 2 )
    {
        printf( "Wrong amount of arguments\n" );
        exit( EXIT_FAILURE );
    }
    else
    {
        char* endp = argv[1];
        N = strtol( argv[1], &endp, 0 );
        if( errno != 0 || *endp != 0 )
        {
            perror( "Getting args" );
            exit( EXIT_FAILURE );
        }
    }

    int qid = 0;
    $(qid = msgget( IPC_PRIVATE, IPC_CREAT | 0600 ));

    for( long i = 1; i <= N; i++ )
    {
        pid_t pid = fork();
        if( pid == 0 )
        {
            child_introduction( qid, i );
        }
    }

    msg_t msg = {};
    msg.msg_n = N + 1;
    $(msgsnd( qid, &msg, 0, 0 ));
    $(msgrcv( qid, &msg, 0, 1, 0 ));

    printf( "\n" );

    $(msgctl( qid, IPC_RMID, NULL ));

    for( long i = 0; i < N; i++ )
        wait( NULL );

    return 0;
}

