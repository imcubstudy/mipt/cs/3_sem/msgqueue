#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <assert.h>
#include <stdint.h>
#include <errno.h>
#include <stdlib.h>

long parseint(const char* _nptr)
{
    char* eptr = NULL;
    errno = 0;

    long res = strtol(_nptr, &eptr, 0);
    if(errno != 0 || *eptr != '\0') {
        errno = EINVAL;
        return -1;
    }
    return res;
}

enum mtpv_t {
    ANYONE  = 1,
    MAN     = 2,
    LOCK_M  = 3,
    WOMAN   = 4,
    LOCK_W  = 5,
    CUB     = 6
};

#define $(code...)                                                  \
    do {                                                            \
        errno = 0;                                                  \
        L fprintf(stderr, "[%d] doing " #code " at " str(__LINE__) "\n",\
                                                    getpid());      \
        code;                                                       \
        if(errno != 0) {                                            \
            perror(#code " at " str(__LINE__));                     \
            exit(EXIT_FAILURE);                                     \
        }                                                           \
    } while(0)
#define L if(0)
#define _str(x) #x
#define str(x) _str(x)

typedef struct {
    long mtyp;
} msg_t;

int q_sync  = 0;
int q_cub   = 0;
int q_lock  = 0;

void human(int _gender);

int main(int argc, char *argv[])
{
    setbuf(stderr, NULL);
    setbuf(stdout, NULL);

    if(argc != 4) {
        errno = EINVAL;
        perror("argc");
        exit(EXIT_FAILURE);
    }

    int N = parseint(argv[1]),
        M = parseint(argv[2]),
        W = parseint(argv[3]);

    $(q_sync = msgget(IPC_PRIVATE, IPC_CREAT | 0600));
    $(q_cub  = msgget(IPC_PRIVATE, IPC_CREAT | 0600));
    $(q_lock = msgget(IPC_PRIVATE, IPC_CREAT | 0600));

    msg_t msg = { .mtyp = CUB };

    for(int i = 0; i < N; ++i)
        msgsnd(q_cub, &msg, 0, 0);

    msg.mtyp = LOCK_M;
    msgsnd(q_lock, &msg, 0, 0);

    msg.mtyp = LOCK_W;
    msgsnd(q_lock, &msg, 0, 0);

    for(int i = 0; i < M + W; ++i)
        switch(fork()) {
            case -1:
                perror("fork");
                exit(EXIT_FAILURE);
            case 0:
                $(human(i < M ? MAN : WOMAN));
                break;
        }

    msg.mtyp = ANYONE;
    $(msgsnd(q_sync, &msg, 0, 0));

    for(int i = 0; i < M + W; ++i)
        $(wait(NULL));

    $(msgctl(q_sync, IPC_RMID, NULL));
    $(msgctl(q_cub,  IPC_RMID, NULL));
    $(msgctl(q_lock, IPC_RMID, NULL));

    return 0;
}

void enter(int _gender)
{
    int lock   = (_gender == MAN) ? LOCK_M : LOCK_W;
    int except = (_gender == MAN) ? WOMAN  : MAN;

    msg_t msg = {};

    $(msgrcv(q_lock, &msg, 0, lock, 0));
    $(msgrcv(q_sync, &msg, 0, except, MSG_EXCEPT));

    if(msg.mtyp == _gender)
        $(msgsnd(q_sync, &msg, 0, 0));

    $(msg.mtyp = _gender);
    $(msgsnd(q_sync, &msg, 0, 0));

    $(msg.mtyp = lock);
    $(msgsnd(q_lock, &msg, 0, 0));
}

void leave(int _gender)
{
    int lock = (_gender == MAN) ? LOCK_M : LOCK_W;

    msg_t msg = {};
    $(msgrcv(q_lock, &msg, 0, lock, 0));

    $(msgrcv(q_sync, &msg, 0, _gender, 0));

    if(msgrcv(q_sync, &msg, 0, _gender, IPC_NOWAIT) == -1 && errno == ENOMSG)
        $(msg.mtyp = ANYONE);
    else
        $(msg.mtyp = _gender);

    $(msgsnd(q_sync, &msg, 0, 0));

    $(msg.mtyp = lock);
    $(msgsnd(q_lock, &msg, 0, 0));
}

void proceed(int _gender)
{
    msg_t msg = {};
    $(msgrcv(q_cub, &msg, 0, CUB, 0));

    pid_t pid = getpid();
    char *gendc = (_gender == MAN) ? " M" : "\tW";

    printf("%s[%d] start\n",  gendc, pid);
    printf("%s[%d] finish\n", gendc, pid);

    $(msgsnd(q_cub, &msg, 0, 0));
}

void human(int _gender)
{
    setbuf(stdout, NULL);
    setbuf(stderr, NULL);

    $(enter(_gender));

    $(proceed(_gender));

    $(leave(_gender));

    $(exit(EXIT_SUCCESS));
}

